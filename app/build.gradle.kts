dependencies {
    implementation(project(":api"))
}

tasks {
    getByName<JacocoReport>("jacocoTestReport") {
        afterEvaluate {
            classDirectories.setFrom(files(classDirectories.files.map {
                fileTree(it) {
                    exclude("org/example/AppClass.class")
                }
            }))
        }
    }
}