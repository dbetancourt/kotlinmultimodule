plugins {
    kotlin("jvm") version "1.3.71"
    jacoco
}

repositories {
    mavenCentral()
}

defaultTasks("clean", "build", "test")

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "jacoco")

    group = "org.example"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
    }

    jacoco {
        toolVersion = "0.8.5"
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8"))
        testImplementation("org.testng:testng:6.14.3")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }

    tasks.withType(org.jetbrains.kotlin.gradle.dsl.KotlinJvmCompile::class) {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    tasks.test {
        useTestNG()
        finalizedBy("jacocoTestReport")
    }
}

tasks.jacocoTestReport {
    val sourceDirs = sourceSets.main.get().allSource.srcDirs
    additionalSourceDirs.setFrom(sourceDirs)
    sourceDirectories.setFrom(sourceDirs)
    classDirectories.setFrom(sourceSets.main.get().output)
    reports {
        html.isEnabled = true
        xml.isEnabled = false
        csv.isEnabled = false
    }
}

tasks.register<JacocoReport>("jacocoRootReport") {
    val sourceDirs = subprojects.map { project -> project.sourceSets.main.get().allSource.srcDirs }
    dependsOn(subprojects.map { project -> project.tasks.test })
    sourceDirectories.setFrom(sourceDirs)
    additionalSourceDirs.setFrom(sourceDirs)
    classDirectories.setFrom(subprojects.map { project -> project.sourceSets.main.get().output })
    executionData.setFrom(subprojects.map { project -> project.tasks.jacocoTestReport.get().executionData })
    reports {
        html.isEnabled = true
        xml.isEnabled = false
        csv.isEnabled = false
    }
}

tasks.test {
    finalizedBy("jacocoRootReport")
}