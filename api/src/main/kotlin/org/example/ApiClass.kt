package org.example

class ApiClass {

    fun foo(x: Int): Int {
        if (x > 50) {
            throw RuntimeException("Error")
        } else {
            return 1
        }
    }
}