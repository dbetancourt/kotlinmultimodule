package org.example

import org.testng.annotations.Test

@Test
class ApiClassTest {

    private val apiClass = ApiClass()

    @Test
    fun test1() {
        val x = apiClass.foo(1)
        assert(x == 1)
    }

    @Test(expectedExceptions = [RuntimeException::class])
    fun test2() {
        apiClass.foo(51)
    }
}